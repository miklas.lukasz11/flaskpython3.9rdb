from flask import Flask
import random
import json
from graphlib import TopologicalSorter
from datetime import date
import revdebug
import requests

from flask import Flask, request

from skywalking import agent, config

# from skywalking import agent
config.init(collector="20.188.46.146:11800", service="Flask_b19")
agent.start()


app = Flask(__name__)

@app.route("/err_piramid")
def err_piramid_err():
    requests.get("http://20.188.46.146:8000/err").content

    


@app.route("/piramid")
def err_piramid():
    return requests.get("http://20.188.46.146:8000/").content

@app.route("/exeption")
def exeption():
    raise Exception("exception")

@app.route("/err_AssertionError")
def err_AssertionError(request):
    raise AssertionError()
    return 'Hello World!'

@app.route("/err_EOFError")
def err_EOFError(request):
    raise EOFError()
    return Response('Hello World!')

@app.route("/err_AttributeError")
def err_AttributeError(request):
    raise AttributeError()
    return Response('Hello World!')

# @app.route("/")
# def lambda_test():
#     x = lambda a: a + 10
#     revdebug.snapshot("/")
#     return str(x(casting_test()))


# def casting_test():
#     x = int("3")
#     w = float("4.2")
#     return x + w


# @app.route("/random")
# def random_test():
#     revdebug.snapshot("random")
#     return str(random.randrange(1, 10, 1))





# @app.route("/exeptions")
# def exeptions():
#     for x in range(10):
#         requests.get("http://20.188.46.146:8000/exeption").content





# @app.route("/json")
# def json():
#     x = '{ "name":"John", "age":30, "city":"New York"}'
#     y = json.loads(x)
#     revdebug.snapshot("json")
#     return y["age"]


# @app.route("/dictionaryMergeUpdateOperators")
# def dictionaryMergeUpdateOperators():
#     x = {"key1": "value1 from x", "key2": "value2 from x"}
#     y = {"key2": "value2 from y", "key3": "value3 from y"}
#     z = x | y
#     print(z)
#     revdebug.snapshot("dictionaryMergeUpdateOperators")


# @app.route("/stringRemovePrefixesAndSuffixes")
# def stringRemovePrefixesAndSuffixes():
#     str = "impossible"
#     str = str.removeprefix("im")
#     print(str)
#     revdebug.snapshot("stringRemovePrefixesAndSuffixes")


# @app.route("/typeHintingGenericsInStandardCollections")
# def typeHintingGenericsInStandardCollections(names: list[str]) -> None:
#     for name in names:
#         print("Hello", name)
#     revdebug.snapshot("typeHintingGenericsInStandardCollections")


# @app.route("/graphLib")
# def graphLib():
#     graph = {"D": {"B", "C"}, "C": {"A"}, "B": {"A"}}
#     ts = TopologicalSorter(graph)
#     print(tuple(ts.static_order()))
#     revdebug.snapshot("graphLib")


# @app.route("/single_for")
# def single_for():
#     test = ""
#     for x in range(3):
#         test = test + "test"
#     revdebug.snapshot("single_for")
#     return test


# @app.route("/nested_for")
# def nested_for():
#     test = ""
#     for x in range(3):
#         test = test + " firstLoops"
#         for x in range(3):
#             test = test + "secondtLoops"
#     revdebug.snapshot("nested_for")
#     return test


# @app.route("/for_each")
# def for_each():
#     test = ""
#     fruits = ["Loopsle", "banana", "cherry"]
#     for x in fruits:
#         test = x
#     revdebug.snapshot("/for_each")
#     return str(test)


# @app.route("/single_while")
# def single_while():
#     test = ""
#     i = 0
#     while i < 10:
#         test = test + "TEST" + str(i) + " "
#         i += 1
#     revdebug.snapshot("single_while")
#     return test


# @app.route("/nested_while")
# def nested_while():
#     test = ""
#     i = 0
#     while i < 3:
#         j = 0
#         while j < 3:
#             test = test + "Test" + str(i) + str(j) + " "
#             j += 1
#         i += 1
#     revdebug.snapshot("nested_while")
#     return test


# @app.route("/returnList")
# def return_list():
#     test = ["alfa", "beta"]
#     return test


# from flask import Flask, url_for
# import requests


# @app.route("/send_requests")
# def send_requests():
#     requests.get("http://20.188.46.146:8000/exeption").content
#     revdebug.snapshot("send_requests" + random_test())
#     return "req"


# @app.route("/site-map")
# def site_map():
#     routes = []
#     links = ""
#     for rule in app.url_map.iter_rules():
#         routes.append("%s" % rule)
#     for x in routes:
#         links = (
#             links + '<a href="http://20.188.46.146:8000' + x + '">' + x + "</a></br>"
#         )
#     return links


# @app.route('/')
# def hello():
#     print('during view')
#     return 'AAAA'

# @app.teardown_request
# def show_teardown(exception):
#     print('after with block')

# with app.test_request_context():
#     print('during with block')

# # teardown functions are called after the context with block exits

# with app.test_client() as client:
#     client.get('/')
#     # the contexts are not popped even though the request ended
#     revdebug.snapshot("Teardown")
#     print(request.path)

# links is now a list of url, endpoint tuples

# import urllib


# @app.route("/urllib")
# def urllib_test():
#     req = urllib.request.Request("ftp://example.com/")
#     print(req.get_full_url)
#     print(req.data)
#     revdebug.snapshot("urllib")
#     return "urllib"


# @app.route("/urllib_request")
# def urllib_request():
#     req = urllib.request.Request("ftp://example.com/")
#     revdebug.snapshot("urllib_request")
#     return "urllib_request"


# @app.route("/urllib_urlopen_wrong_url")
# def urllib_urlopen_wrong_url():
#     req = urllib.request.urlopen("wrong_url")
#     print(req.get_full_url)
#     print(req.data)
#     revdebug.snapshot("urllib_urlopen_wrong_url")
#     return "urllib_urlopen_wrong_url"


# @app.route("/urllib_urlopen")
# def urllib_urlopen():
#     req = urllib.request.urlopen("http://20.188.46.146:8000")
#     req.get_full_url
#     # print(req.data)
#     revdebug.snapshot("urllib_urlopen")
#     return "urllib_urlopen"


# import urllib3


# @app.route("/urllib3")
# def urllib3_test():
#     http = urllib3.PoolManager()
#     r = http.request("GET", "http://httpbin.org/robots.txt")
#     print(r.status)
#     print(r.data)
#     revdebug.snapshot("urllib3")
#     return "urllib3"


# @app.route("/urllib3_request")
# def urllib3_request():
#     http = urllib3.PoolManager()
#     r = http.request("POST", "http://httpbin.org/post", fields={"hello": "world"})
#     print(r.status)
#     print(r.data)
#     revdebug.snapshot("urllib3_request")
#     return r.data


# import io

# @app.route("/urllib3_io_test")
# def io_test():
#     http = urllib3.PoolManager()
#     r = http.request('GET', 'https://example.com', preload_content=False)
#     r.auto_close = False
#     html=''
#     for line in io.TextIOWrapper(r):
#         html=html+line
#     revdebug.snapshot("urllib3_io_test")
#     return html

# @app.route("/urllib3_files")
# def urllib3_files():
#     f = open("example.txt", "w")
#     f.write("Now the file has more content!")
#     f.close()

#     http = urllib3.PoolManager()
#     with open('example.txt') as fp:
#         file_data = fp.read()
#         r = http.request('POST','http://httpbin.org/post',fields={'filefield': ('example.txt', file_data),})
#         #json.loads(r.data.decode('utf-8'))['files']
#         revdebug.snapshot("urllib3_files")
#     return "urllib3_files"


# import certifi
# @app.route("/urllib3_Certificate_Verification")
# def urllib3_Certificate_Verification():
#     http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
#     print(http.request('GET', 'https://google.com'))
#     http.request('GET', 'https://expired.badssl.com')
#     revdebug.snapshot("urllib3_Certificate_Verification")
#     return "urllib3_Certificate_Verification"


# @app.route("/requests")
# def requests_test():
#     r = requests.get('https://api.github.com/user', auth=('user', 'pass'))
#     r.json()
#     print(r.content)
#     print(r.text)
#     print(r.encoding)
#     print(r.headers['content-type'])
#     print(r.status_code)
#     revdebug.snapshot("requests")
#     return r.content

# import os
# @app.route("/delete_file")
# def delete_file():
#     f = open("example.txt", "w")
#     f.write("Now the file has more content!")
#     f.close()
#     if os.path.exists("example.txt"):
#         os.remove("example.txt")
#     else:
#         print("The file does not exist")
#     revdebug.snapshot("delete_file")
#     return 'delete file'

# @app.route("/User_Input")
# def User_Input():
#     username = input("Enter username:")
#     print("Username is: " + username)
#     revdebug.snapshot("User_Input")
#     return 'User_Input'


# import threading

# def coder(number):
#     print ('Coders:  ' , number)
#     revdebug.snapshot("threading_test_"+str(number))
#     return


# @app.route("/threading_test")
# def threading_test():
#     threads = []
#     for k in range(5):
#         t = threading.Thread(target=coder, args=(k,))
#         threads.append(t)
#         t.start()
#     revdebug.snapshot("threading_test")
#     return 'threading_test'

# import pymongo


# @app.route("/Create_Database")
# def Create_Database():
#     myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#     mydb = myclient["mydatabase"]
#     print(mydb)
    
#     revdebug.snapshot("Create_Database")
#     return 'Create_Database'

# @app.route("/list_database_names")
# def list_database_names():
#     myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#     print(myclient)
    
#     revdebug.snapshot("list_database_names")
#     return 'list_database_names'

# @app.route("/url error*@&#(&)!`~~~~     \n")
# def url_err():
#     revdebug.snapshot("url_err")
#     return 'url_err'

# import requests

# @app.route("/status_403")
# def status_403():
#     raise HTTPError(403)
#     revdebug.snapshot("status_403")
#     return 'status_403'