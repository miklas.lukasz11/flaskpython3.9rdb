from flask import Flask
import random
import json
from graphlib import TopologicalSorter
from datetime import date
import revdebug
import requests

from flask import Flask, request

from skywalking import agent, config

# from skywalking import agent
config.init(collector="20.188.46.146:11800", service="Flask_b20")
agent.start()


app = Flask(__name__)


@app.route("/err_AssertionError")
def err_AssertionError():
    raise AssertionError()
    return 'Hello World!'
    
@app.route("/err_EOFError")
def err_EOFError():
    raise EOFError()
    return 'Hello World!'
    
@app.route("/err_AttributeError")
def err_AttributeError():
    raise AttributeError()
    return 'Hello World!'
    
@app.route("/err_FloatingPointError")
def err_FloatingPointError():
    raise FloatingPointError()
    return 'Hello World!'
    
@app.route("/err_GeneratorExit")
def err_GeneratorExit():
    raise GeneratorExit()
    return 'Hello World!'
    
@app.route("/err_ImportError")
def err_ImportError():
    raise ImportError()
    return 'Hello World!'
    
@app.route("/err_ModuleNotFoundError")
def err_ModuleNotFoundError():
    raise ModuleNotFoundError()
    return 'Hello World!'
    
@app.route("/err_IndexError")
def err_IndexError():
    raise IndexError()
    return 'Hello World!'
    
@app.route("/err_KeyError")
def err_KeyError():
    raise KeyError()
    return 'Hello World!'
    
@app.route("/err_KeyboardInterrupt")
def err_KeyboardInterrupt():
    raise KeyboardInterrupt()
    return 'Hello World!'
    
@app.route("/err_MemoryError")
def err_MemoryError():
    raise MemoryError()
    return 'Hello World!'
    
@app.route("/err_NameError")
def err_NameError():
    raise NameError()
    return 'Hello World!'
    
@app.route("/err_OSError")
def err_OSError():
    raise OSError()
    return 'Hello World!'
    
@app.route("/err_OverflowError")
def err_OverflowError():
    raise OverflowError()
    return 'Hello World!'

@app.route("/err_NotImplementedError")    
def err_NotImplementedError():
    raise NotImplementedError()
    return 'Hello World!'

@app.route("/err_RecursionError")    
def err_RecursionError():
    raise RecursionError()
    return 'Hello World!'

@app.route("/err_ReferenceError")    
def err_ReferenceError():
    raise ReferenceError()
    return 'Hello World!'
    
@app.route("/err_RuntimeError")
def err_RuntimeError():
    raise RuntimeError()
    return 'Hello World!'
    
@app.route("/err_StopIteration")
def err_StopIteration():
    raise StopIteration()
    return 'Hello World!'
    
@app.route("/err_StopAsyncIteration")
def err_StopAsyncIteration():
    raise StopAsyncIteration()
    return 'Hello World!'
    
@app.route("/err_SyntaxError")
def err_SyntaxError():
    raise SyntaxError()
    return 'Hello World!'
    
@app.route("/err_IndentationError")
def err_IndentationError():
    raise IndentationError()
    return 'Hello World!'
    
@app.route("/err_TabError")
def err_TabError():
    raise TabError()
    return 'Hello World!'
    
@app.route("/err_SystemError")
def err_SystemError():
    raise SystemError()
    return 'Hello World!'
    
@app.route("/err_SystemExit")
def err_SystemExit():
    raise SystemExit()
    return 'Hello World!'
    
@app.route("/err_TypeError")
def err_TypeError():
    raise TypeError()
    return 'Hello World!'
    
@app.route("/err_UnboundLocalError")
def err_UnboundLocalError():
    raise UnboundLocalError()
    return 'Hello World!'
    
@app.route("/err_UnicodeError")
def err_UnicodeError():
    raise UnicodeError()
    return 'Hello World!'
    
@app.route("/err_UnicodeEncodeError")
def err_UnicodeEncodeError():
    raise UnicodeEncodeError()
    return 'Hello World!'
    
@app.route("/err_UnicodeDecodeError")
def err_UnicodeDecodeError():
    raise UnicodeDecodeError()
    return 'Hello World!'
    
@app.route("/err_UnicodeTranslateError")
def err_UnicodeTranslateError():
    raise UnicodeTranslateError()
    return 'Hello World!'
    
@app.route("/err_ValueError")
def err_ValueError():
    raise ValueError()
    return 'Hello World!'
    
@app.route("/err_ZeroDivisionError")
def err_ZeroDivisionError():
    raise ZeroDivisionError()
    return 'Hello World!'
    
@app.route("/err_EnvironmentError")
def err_EnvironmentError():
    raise EnvironmentError()
    return 'Hello World!'
    
@app.route("/err_IOError")
def err_IOError():
    raise IOError()
    return 'Hello World!'
    
@app.route("/err_WindowsError")
def err_WindowsError():
    raise WindowsError()
    return 'Hello World!'

@app.route("/err_BlockingIOError")#os exceptions
def err_BlockingIOError():
    raise BlockingIOError()
    return 'Hello World!'
    
@app.route("/err_ChildProcessError")
def err_ChildProcessError():
    raise ChildProcessError()
    return 'Hello World!'
    
@app.route("/err_ConnectionError")
def err_ConnectionError():
    raise ConnectionError()
    return 'Hello World!'
    
@app.route("/err_BrokenPipeError")
def err_BrokenPipeError():
    raise BrokenPipeError()
    return 'Hello World!'
    
@app.route("/err_ConnectionAbortedError")
def err_ConnectionAbortedError():
    raise ConnectionAbortedError()
    return 'Hello World!'
    
@app.route("/err_ConnectionRefusedError")
def err_ConnectionRefusedError():
    raise ConnectionRefusedError()
    return 'Hello World!'
    
@app.route("/err_ConnectionResetError")
def err_ConnectionResetError():
    raise ConnectionResetError()
    return 'Hello World!'
    
@app.route("/err_FileExistsError")
def err_FileExistsError():
    raise FileExistsError()
    return 'Hello World!'
    
@app.route("/err_FileNotFoundError")
def err_FileNotFoundError():
    raise FileNotFoundError()
    return 'Hello World!'
    
@app.route("/err_InterruptedError")
def err_InterruptedError():
    raise InterruptedError()
    return 'Hello World!'
    
@app.route("/err_IsADirectoryError")
def err_IsADirectoryError():
    raise IsADirectoryError()
    return 'Hello World!'

@app.route("/err_NotADirectoryError")    
def err_NotADirectoryError():
    raise NotADirectoryError()
    return 'Hello World!'
    
@app.route("/err_PermissionError")
def err_PermissionError():
    raise PermissionError()
    return 'Hello World!'
    
@app.route("/err_ProcessLookupError")
def err_ProcessLookupError():
    raise ProcessLookupError()
    return 'Hello World!'
    
@app.route("/err_TimeoutError")
def err_TimeoutError():
    raise TimeoutError()
    return 'Hello World!'

